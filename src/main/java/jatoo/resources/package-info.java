/**
 * A Java™ Open Source library created to ease the work with various resources
 * (like locale specific texts, images, icons, etc).
 * 
 * @author <a href="http://cristian.sulea.net" rel="author">Cristian Sulea</a>
 */
package jatoo.resources;
