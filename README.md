

# JaToo Resources

A Java� Open Source library created to ease the work with various resources (like locale specific texts, images, icons, etc).

**GitHub**

* [Code](https://github.com/cristian-sulea/jatoo-resources)
* [Releases](https://github.com/cristian-sulea/jatoo-resources/releases)
* [Issues](https://github.com/cristian-sulea/jatoo-resources/issues)
* [Pull requests](https://github.com/cristian-sulea/jatoo-resources/pulls)

**SourceForge.net**

* [Summary](http://sourceforge.net/projects/jatoo-resources/)
* [Files](http://sourceforge.net/projects/jatoo-resources/files/)
* [Support](http://sourceforge.net/projects/jatoo-resources/support/)


## Contact

To contact project administrator visit personal web page:
		
* [http://cristian.sulea.net](http://cristian.sulea.net "Cristian Sulea")

Or one of the profile pages:

* [https://github.com/cristian-sulea](@ GitHub "Cristian Sulea")
* [http://sourceforge.net/users/cristian-sulea/](@ SourceForge.net "Cristian Sulea")


## Licensing

Copyright (C) Cristian Sulea ( [http://cristian.sulea.net](http://cristian.sulea.net "Cristian Sulea") )

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

